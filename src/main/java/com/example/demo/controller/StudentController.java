package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("student")
public class StudentController {

	@RequestMapping("show")
	public String getData()
	{
		return "Name : Raj, Address : Pune";
	}
}
